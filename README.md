# Finding related sequences by a simple sum over alignments

These files support the study titled [A simple theory for finding
related sequences by adding probabilities of alternative
alignments][paper].  See there for more details.

## Alignment parameters from gap probabilities

`gap-params-from-probs` is a simple Python script that calculates
alignment parameters from gap probabilities.  The input is either 3 or
5 probabilities:

    alpha beta finishProb

or:

    alpha_D alpha_I beta_D beta_I finishProb

The former means that `alpha_D = alpha_I` and `beta_D = beta_I`.
These probabilities are defined in Figure 5A of [btz576][].
"finishProb" means: `1 - gamma - alpha_D - alpha_I`.

In short, alpha is the gap start probability, beta is the gap
extension probability, and finishProb is the probability of the
alignment finishing.  The D and I versions allow different
probabilities for deletion and insertion.

Example usage:

    gap-params-from-probs 0.05 0.7 0.01

The script calculates `omega_D` and `omega_I`, by assuming that they
are equal, and finding the unique value of `omega = omega_D = omega_I`
that satisfies equation 12 in [btz576][].  It then calculates
parameters defined in section 5.1 of [btz576][].

## Consensus sequences of amniote repeats

`dfam3.7-amniota.fa` has ancient repeat sequences that were used in
the study.

## last-train output files

The files ending in `.train` were made by `last-train`.  They show the
rates of matches, mismatches, and gaps found at each training
iteration.  This is explained in the [last-train][] doc, especially
the "Details" section.

[btz576]: https://doi.org/10.1093/bioinformatics/btz576
[last-train]: https://gitlab.com/mcfrith/last/-/blob/main/doc/last-train.rst
[paper]: https://doi.org/10.1101/2023.09.26.559458
